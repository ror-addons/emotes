--OBJEKTKONSTRUKT*******************************************************************
local PLAYERNAME = tostring(wstring.gsub( GameData.Player.name, L"(^.)", L"" ))

if Ebutton == nil then Ebutton = {} end

function Ebutton:New() 
    local o = {} 
    setmetatable (o, { __index = self } )	
    return o 
end

function Ebutton:init(sammler,id)
	self.id = id
	self.collector = sammler
	self.name = sammler.itemname..id
	sammler:add(self)
	self.index = sammler.itemcount
	self.parent = sammler.parent
	self.relativeto = sammler.relativeto
    self.vorlage = sammler.vorlage
    self.savevarprefix = sammler.savevarprefix
    self.state = false
	self.bezeichnung="LEERNAME"
    self.beschreibung="LEERBESCHREIBUNG"
    self.spalte = 0
    self.zeile = 0
    self.shift = false
    self.icon=sammler.icon
    self.userlevel=sammler.userlevel
    self.abstand={sammler.abstand[1],sammler.abstand[2],sammler.abstand[3],sammler.abstand[4]}
    self.pos = {0,0}
	self.drawed = false
	self.collectorzeilen = sammler.zeilen
    self.collectorspalten = sammler.spalten
    
    self.register = sammler.register
 	self:triggercopy(sammler.triggers)
end

function Ebutton:kill()

	self:draw(false) 
	self.collector:destroy(self)
	self.collector= nil
	self = nil	

end

function Ebutton:doregister(an)
	if an then
	   LayoutEditor.RegisterWindow( self.name,   towstring(self.bezeichnung), towstring(self.beschreibung) ,   false, false,   true, nil )
	else
	   LayoutEditor.UnregisterWindow( self.name )
	end
end

function Ebutton:triggercopy(obj)
    if self.triggers == nil then self.triggers={} end
	for k,v in pairs(obj) do
		 table.insert(self.triggers,v)
	end
	
end

function Ebutton:toggle(an)
    if an == nil then self.state= not self.state
    else self.state = an end     
    self:trigger()
end

function Ebutton:trigger()
    for k,v in pairs(self.triggers) do v(self) end
    --for k,v in pairs(self.triggers) do self:triggers[k]() end
end

function Ebutton:addtrigger(ntrg)
	--d("ADDDDTRIGGER",self.triggers)
    if self.triggers == nil then self.triggers={} end
	table.insert(self.triggers,ntrg)
end

function Ebutton:saveSetting(val)
	--d("save:   "..PLAYERNAME..self.savevarprefix..self.id..":  "..tostring(val) )
    emotes.Settings[PLAYERNAME..self.savevarprefix..self.id]= val
end

function Ebutton:loadSetting()
	--d("load:   "..PLAYERNAME..self.savevarprefix..self.id )
    return emotes.Settings[PLAYERNAME..self.savevarprefix..self.id] or false
end

function Ebutton:move(an)
	if not self.register then return end
	if an==nil then	an = not self.moveable end
	WindowSetMovable(self.name,an)
	self.moveable = an
end

function Ebutton:SetPos()
	WindowClearAnchors(self.name)
	--d("--------------",self.abstand,self.spalte,self.zeile)
	--self.bezeichnung = towstring(self.spalte)..L"<spalte   zeile>"..towstring(self.zeile)
	
	if self.collector.vertical then
		self.pos[2]=  self.abstand[1] + (self.abstand[4]*self.spalte)
	    self.pos[1]=  self.abstand[2] + (self.abstand[3]*self.zeile)
    else
	    self.pos[1]=  self.abstand[2] + (self.abstand[3]*self.spalte)
	    self.pos[2]=  self.abstand[1] + (self.abstand[4]*self.zeile)
    end
    --LabelSetText (self.name.."Label", towstring(self.pos[1])..L"   <>   "..towstring(self.pos[2]))
	--d("--------------",self.pos)
	WindowAddAnchor(self.name, "topleft", self.relativeto, "topleft", self.pos[1], self.pos[2])
	self:move(self.moveable)
end

function Ebutton:SetGridPos()

	if self.collector.zeilen ~= 0 or self.collector.spalten ~= 0 then				    

		local iindex = self.index
		if self.collector.vertical then
				self.spalte = math.floor(iindex / self.collector.zeilen)
		    	self.zeile =  iindex - (self.spalte*self.collector.zeilen) 
		else
		        self.zeile = math.floor(iindex / self.collector.spalten) 
		    	self.spalte =  iindex - (self.zeile*self.collector.spalten) 
		end
        
    end 
end

function Ebutton:GetPos()
	if WindowGetAnchorCount( self.name ) > 0 then
	    local point,relpoint,relwin,x,y = WindowGetAnchor(self.name, 1)
	    return {x,y}
	else
		return false
	end
end



function Ebutton:draw(an) 
 
	if an and not self.drawed then
		--d("draw-->"..self.name,self.register,self.parent)
		CreateWindowFromTemplate(self.name, self.vorlage, self.parent)
		self.drawed = true
	    --self.index = self.collector.drawcount
        self:drawSubElements()
        
        WindowSetShowing(self.name,true)
        self.collector.drawcount = self.collector.drawcount +1

	elseif self.drawed then
		self:doregister(false)
		DestroyWindow(self.name)
		self.collector.drawcount = self.collector.drawcount -1
		self.drawed = false
	
	end
end

--ACTIONS###########################################################################


function Ebutton:Tooltip(obj)

    Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, towstring(self.beschreibung))
    Tooltips.Finalize()
    Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_LEFT )

end

function Ebutton:drawSubElements()
end
  




--GRUPPIERUNG###########################################################################


Egroup = {}

function Egroup:New() 
    local o = {} 
    setmetatable (o, { __index = self } )	
    return o 
end

function Egroup:init(suchindex)

	self.suchindex = suchindex
	self.itemcount=0
	self.drawcount=0
	self.items = {}
	self.parent = "Root"
	self.relativeto = "Root"
	self.name = "TEMP"
    self.state = false
	self.bezeichnung="LEERNAME"
    self.beschreibung="LEERBESCHREIBUNG"
    self.icon=10942
    self.userlevel=1
    self.abstand = {0,0,0,0}
    self.register = false
    self.vertical = false
    self.spalten = 0
    self.zeilen = 0
    self.dependent = false
    self.triggers={}
	   

end
		 
function Egroup:add(item)   	
	self.items[item.id]=item
	self.suchindex[item.name]=item
	self.itemcount=self.itemcount+1 
end

function Egroup:destroy(item)   	
	self.items[item.id]=nil
	self.suchindex[item.name]=nil
	self.itemcount=self.itemcount-1
	--d("DESTROY COLLECTOR",item.id) 
end

function Egroup:calcDimensions()
	if self.zeilen ~= 0 or self.spalten ~= 0 then				    

		local iindex = self.drawcount
		if self.vertical then
				self.spalten = math.ceil(iindex / self.zeilen)
		else
		        self.zeilen = math.ceil(iindex / self.spalten) 
		end

    end 
end

function Egroup:sizeWindow()
		self:calcDimensions()
		local breite = (self.abstand[3]*(self.spalten))+self.abstand[1]
		local hoehe = (self.abstand[4]*(self.zeilen))+self.abstand[2]
		WindowSetDimensions(self.parent,breite,hoehe)
end


function Egroup:DrawElements(on,trigger)
    for zeile, Element in pairs(self.items)
    do
        if self.dependent then
        	todraw = self.dependent[Element.id].state
		end
		Element:draw(on);
		if trigger then Element:trigger() end
    end
	self:sizeWindow()
end

function Egroup:posElements()
    local index = 0
	for zeile, Element in pairs(self.items)
    do
		if Element.drawed then
			Element.index = index
			index = index + 1
			Element:SetGridPos()
			if Element.register then
				Element:doregister(true)
				if Element:GetPos() == false then
					Element.moveable = true
					Element:SetPos() 
				end
			else
				Element:SetPos() 
			end
		end
   end
end 


function Egroup:trigger()
    for k,v in pairs(self.triggers) do v(self) end
end

function Egroup:additemtrigger(ntrg)
	--d("ADDDDTRIGGER",self.triggers)
    if self.triggers == nil then self.triggers={} end
	table.insert(self.triggers,ntrg)
end

		 
		 
	 