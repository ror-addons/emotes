<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="emotes" version="1.3" date="" >
		
        <Dependencies>

 <!--            <Dependency name="compass" />
            <Dependency name="EA_ActionBars" />
            <Dependency name="EA_TargetWindow" />
			<Dependency name="SharedAssetsSquared" />-->
        </Dependencies>
        <Author name="GutGut" email="curseforge" />
		<Description text="Emote GUI - thanks to secure for the update of the emotelist" />
		<VersionSettings gameVersion="1.5.5" windowsVersion="1.0" savedVariablesVersion="1.0" /> 
        
		<Files>
						
			<File name="emotesVorlagen.xml" />
			<File name="emotes.xml" />						
			<File name="emotesOBJECT.lua" />
			<File name="emotes.lua" />
		
			
		</Files>
		
		<OnInitialize>
			<CallFunction name="emotes.Initalize" />
		</OnInitialize>
		
      <SavedVariables>
         <SavedVariable name="emotes.Settings" />
      </SavedVariables>
      
<!--    <OnUpdate>
          <CallFunction name="emotes.OnUpdate" />
      </OnUpdate>-->
		
	</UiMod>
</ModuleFile>

   