--LOCAL*********************************************************

local WINDOW_BUTTONS = "emotesDisplayWindow"
local WINDOW_OPTIONS = "emotesFilterWindow" 

													-- Hauptfenster (Einstellungsknopf)	
local SINGLE_BUTTON = WINDOW_BUTTONS.."Button"
local SINGLE_OPTION = WINDOW_OPTIONS.."Option"

	                                    
local VORLAGE_BUTTON = "emotes_Entry"
local VORLAGE_OPTION = "emotes_MapPinFilterEntry"                                          -- FilterFenster


local ABSTAND_OPTIONS = {10,10,210,30}
local ABSTAND_BUTTONS = {10,10,190,30}

                                               -- [1]=basisverschiebungx  [2]=basisverschiebungy [3]=abstandx [4]abstandy
local BUTTON_AN_COLOR = {r=255,g=255,b=255}
local BUTTON_AUS_COLOR = {r=155,g=155,b=155}					                                
local USERMODE = 3  																-- 3=debug 2=erweitert 1=normal
local TIMESTAMP = 0
local TIME = 0


--VARIABLEN*********************************************************

emotes = {}
emotes.Settings = {}
emotes.Options = {}
emotes.Buttons = {}
emotes.Sindex = {}

local mi = false
local bu = false
local gr = false




			   
--GRUPPE*********************************************************

	emotes.Options = Egroup:New()
	emotes.Options:init(emotes.Sindex)
	emotes.Options.bezeichnung="Optionen"
	emotes.Options.itemname=SINGLE_OPTION
	emotes.Options.userlevel=1
	emotes.Options.parent = WINDOW_OPTIONS
	emotes.Options.relativeto = WINDOW_OPTIONS
	emotes.Options.vorlage = VORLAGE_OPTION
	emotes.Options.savevarprefix = "Option_"
	emotes.Options.register = false
	emotes.Options.abstand = ABSTAND_OPTIONS
	emotes.Options.spalten = 4
	emotes.Options:additemtrigger(function(self) ButtonSetPressedFlag( self.name.."Checkbox", self.state ) end)
    emotes.Options:additemtrigger(function(self) self:saveSetting(self.state) end)
			   
--GRUPPE*********************************************************

	emotes.Buttons = Egroup:New()
	emotes.Buttons:init(emotes.Sindex)
	emotes.Buttons.bezeichnung="Buttons"
	emotes.Buttons.itemname=SINGLE_BUTTON
	emotes.Buttons.userlevel=1
	emotes.Buttons.parent = WINDOW_BUTTONS
	emotes.Buttons.relativeto = WINDOW_BUTTONS
	emotes.Buttons.vorlage = VORLAGE_BUTTON
	emotes.Buttons.savevarprefix = "Setting_"
	emotes.Buttons.register = false
	emotes.Buttons.abstand = ABSTAND_BUTTONS
	emotes.Buttons.spalten = 4
	emotes.Buttons.dependent = emotes.Options.items










mi = nil
bu= nil

function emotes.Initalize()
    CreateWindow("emotesWindow", true)
	CreateWindow(WINDOW_OPTIONS, false)
	CreateWindow(WINDOW_BUTTONS, false)

	
	if SystemData.Settings.Language.active == 3 then
	    emotes.items=emotes.GERMAN
	else
		emotes.items=emotes.ENGLISH
	end

  	LayoutEditor.RegisterWindow("emotesWindow",   L"emotesWindow",   L"emotesWindow",   false, false,   true, nil )
    Ebutton.drawSubElements=drawSubElements
    
	 for k,v in pairs(emotes.items) do
		
		mi = Ebutton:New()
		mi:init(emotes.Options,tostring(k))
		mi.state = mi:loadSetting()
		mi.bezeichnung=v[2]
		mi.beschreibung=v[2]
		mi.icon=23166
		mi.userlevel=1
		bu = Ebutton:New()
		bu:init(emotes.Buttons,tostring(k))
		bu.bezeichnung=v[2]
		bu.beschreibung=v[2]
		bu.icon=23166
		bu:addtrigger(function(self) 
		    --SystemData.UserInput.ChatText = towstring(v[1]),L""; 
	      	--BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT );
	      	SendChatText(towstring(v[1]),L"")
		end)
		mi:addtrigger(function(self) emotes.Buttons.items[self.id]:draw(self.state); emotes.Buttons:posElements(); emotes.Buttons:sizeWindow(); end)
	    mi:draw(true)
	    mi:trigger(mi.state)
	    
			
	end   
    
      
    emotes.Options:sizeWindow()
    emotes.Buttons:sizeWindow()
    emotes.Options:posElements()
    emotes.Buttons:posElements()
    
    
end


--EVENTS*********************************************************************

function emotes.MouseoverItem()
    emotes.Sindex[SystemData.ActiveWindow.name]:Tooltip()
end

function emotes.ToggleFilteremotes()
    WindowUtils.ToggleShowing( "emotesFilterWindow" )
end

function emotes.Toggleemotes()
    WindowUtils.ToggleShowing( "emotesDisplayWindow" )
end

function emotes.Toggle()
	local p = emotes.Sindex[SystemData.ActiveWindow.name]
	LabelSetTextColor(p.name.."Label", BUTTON_AN_COLOR.r, BUTTON_AN_COLOR.g, BUTTON_AN_COLOR.b)
	p:toggle()
end

function emotes.DownToggle()
	local p = emotes.Sindex[SystemData.ActiveWindow.name]
	LabelSetTextColor(p.name.."Label", BUTTON_AUS_COLOR.r, BUTTON_AUS_COLOR.g, BUTTON_AUS_COLOR.b)
		--WindowSetTintColor(p.name.."Icon",125,125,125);
end


function emotes.OnMouseOverFilteremotesButton()
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, towstring("emotes Einstellungen"))
	Tooltips.Finalize()
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_LEFT )
end

function emotes.OnUpdate()
	 local t = GetComputerTime()
	 if t == TIMESTAMP then return end
     TIMESTAMP = t
end


function drawSubElements(self,...)

    --local texture, x, y = GetIconData( self.icon )
	--DynamicImageSetTexture( self.name.."Icon", texture, x, y )
	LabelSetText (self.name.."Label", towstring(self.bezeichnung))


end

emotes.ENGLISH={
{"/agree","agree"},
{"/beckon","beckon"},
{"/beg","beg"},
{"/boast","boast"},
{"/bored","bored"},
{"/bow","bow"},
{"/brandish","brandish"},
{"/burp","burp"},
{"/bye","bye"},
{"/charge","charge"},
{"/cheer","cheer"},
{"/chicken","chicken"},
{"/clap","clap"},
{"/cower","cower"},
{"/cry","cry"},
{"/dance","dance"},
{"/disagree","disagree"},
{"/drink","drink"},
{"/drunk","drunk"},
{"/eat","eat"},
{"/fart","fart"},
{"/flex","flex"},
{"/frown","frown"},
{"/glare","glare"},
{"/gloat","gloat"},
{"/grumble","grumble"},
{"/halt","halt"},
{"/happy","happy"},
{"/hello","hello"},
{"/howl","howl"},
{"/impatient","impatient"},
{"/insult","insult"},
{"/intimidate","intimidate"},
{"/kata","kata"},
{"/laugh","laugh"},
{"/lol","lol"},
{"/mad","mad"},
{"/no","no"},
{"/pickteeth","pickteeth"},
{"/pig","pig"},
{"/point","point"},
{"/ponder","ponder"},
{"/pray","pray"},
{"/puke","puke"},
{"/rude","rude"},
{"/salute","salute"},
{"/scratch","scratch"},
{"/scream","scream"},
{"/showoff","showoff"},
{"/shrug","shrug"},
{"/shy","shy"},
{"/slit","slit"},
{"/smile","smile"},
{"/special","special"},
{"/stomp","stomp"},
{"/swear","swear"},
{"/talk","talk"},
{"/taunt","taunt"},
{"/thank","thank"},
{"/thanks","thanks"},
{"/think","think"},
{"/toast","toast"},
{"/tome","tome"},
{"/waaagh","waaagh"},
{"/warcry","warcry"},
{"/warhammer","warhammer"},
{"/wave","wave"},
{"/weep","weep"},
{"/whistle","whistle"},
{"/yawn","yawn"},
{"/yes","yes"},
{"/angry","angry"},
{"/bang","bang"},
{"/bite","bite"},
{"/blank","blank"},
{"/blush","blush"},
{"/bow","bow"},
{"/cards","cards"},
{"/chuckle","chuckle"},
{"/confused","confused"},
{"/congrats","congrats"},
{"/crossfingers","crossfingers"},
{"/curtsey","curtsey"},
{"/dismiss","dismiss"},
{"/doh","doh"},
{"/facepalm","facepalm"},
{"/flirt","flirt"},
{"/gamble","gamble"},
{"/groom","groom"},
{"/gtg","gtg"},
{"/heal","heal"},
{"/hug","hug"},
{"/kiss","kiss"},
{"/lmao","lmao"},
{"/look","look"},
{"/poke","poke"},
{"/praise","praise"},
{"/prance","prance"},
{"/preen","preen"},
{"/present","present"},
{"/pwnd","pwnd"},
{"/rage","rage"},
{"/raise","raise"},
{"/ready","ready"},
{"/really","really?"},
{"/rez","rez"},
{"/rofl","rofl"},
{"/shiver","shiver"},
{"/slap","slap"},
{"/stagger","stagger"},
{"/surrender","surrender"},
{"/sweat","sweat"},
{"/victory","victory"},
{"/woot","woot"},
{"/wow","wow"},
{"/yeehaw","yeehaw"},
{"/zombie","Zombie"}

}


emotes.GERMAN={
{"/agree","zustimmen"},
{"/beckon","herbeiwinken"},
{"/beg","betteln"},
{"/boast","prahlen "},
{"/bored","gelangweilt"},
{"/bow","verbeugen"},
{"/brandish","mit Waffe fuchteln"},
{"/burp","r�lpsen"},
{"/bye","tsch��"},
{"/charge","st�rm los!"},
{"/cheer","jubeln"},
{"/chicken","wie H�hnchen benehmen"},
{"/clap","klatschen"},
{"/cower","kauern"},
{"/cry","weinen"},
{"/dance","tanzen"},
{"/disagree","widersprechen"},
{"/drink","trinken"},
{"/drunk","betrinken"},
{"/eat","essen"},
{"/fart","furzen"},
{"/flex","Muskeln spielen lassen"},
{"/frown","Stirnrunzeln"},
{"/glare","zornig starren"},
{"/gloat","h�misch freuen"},
{"/grumble","grummeln"},
{"/halt","Halt signalisieren"},
{"/happy","gl�cklich"},
{"/hello","gr��en"},
{"/howl","heulen"},
{"/impatient","ungeduldig"},
{"/insult","beleidigen"},
{"/intimidate","bedrohen"},
{"/kata","Kampfk�nste zeigen"},
{"/laugh","lachen"},
{"/lol","laut lachen"},
{"/mad","genervt "},
{"/no","Kopf sch�tteln"},
{"/pickteeth","Z�hne stochern"},
{"/pig","wie ein Schwein benehmen"},
{"/point","zeigen"},
{"/ponder","gr�beln"},
{"/pray","beten"},
{"/puke","kotzen"},
{"/rude","unanst�ndige Geste"},
{"/salute","salutieren"},
{"/scratch","kratzen"},
{"/scream","schreien"},
{"/showoff","angeben"},
{"/shrug","mit Schultern zucken"},
{"/shy","sch�chtern"},
{"/slit","Kehle aufschlitzen"},
{"/smile","l�cheln"},
{"/special","etwas Besonderes machen"},
{"/stomp","aufstampfen"},
{"/swear","fluchen"},
{"/talk","reden"},
{"/taunt","spotten"},
{"/thank","danken"},
{"/thanks","bedanken"},
{"/think","denken"},
{"/toast","Trinkspruch"},
{"/tome","W�lzer"},
{"/waaagh","Waaagh!"},
{"/warcry","Schlachtruf"},
{"/warhammer","Warhammer-Geste"},
{"/wave","winken"},
{"/weep","weinen"},
{"/whistle","pfeifen"},
{"/yawn","g�hnen"},
{"/yes","zustimmen"},
{"/angry","b�se angucken"},
{"/bang","aufmerksamkeit erregen"},
{"/bite","bei�en"},
{"/blank","ausdruckslos anstarren"},
{"/blush","err�ten"},
{"/bow","verbeugen"},
{"/cards","Karten spielen"},
{"/chuckle","kichern"},
{"/confused","verwirrt"},
{"/congrats","begl�ckw�nschen"},
{"/crossfingers","Daumen dr�cken"},
{"/curtsey","einen Knicks machen"},
{"/dismiss","entlassen"},
{"/doh","hand vor den kopf schlagen, begreifen"},
{"/facepalm","ECKELN, gesicht mit den h�nden verdecken"},
{"/flirt","flirten /sch�kern, kokettieren"},
{"/gamble","gl�cksspiel"},
{"/groom","hygiene komentieren"},
{"/gtg","startklar"},
{"/heal","um heilung bitten"},
{"/hug","UMARMEN"},
{"/kiss","kusshand"},
{"/lmao","vor lachen umkippen"},
{"/look","anschauen / in die ferne blicken"},
{"/poke","anknuffen / in der luft herumstochern"},
{"/praise","loben"},
{"/prance","herumt�nzeln"},
{"/preen","SICH BR�STEN"},
{"/present","geschichte schildern"},
{"/pwnd","Sieg feiern"},
{"/rage","w�tend sein"},
{"/raise","Hand heben"},
{"/ready","bereit sein"},
{"/really","wirklich? / fragend ansehen"},
{"/rez","Wiederbelebung erbitten"},
{"/rofl","vor lachen auf dem Boden rollen"},
{"/shiver","zittern"},
{"/slap","jmdn./etw. schlagen"},
{"/stagger","TAUMELN"},
{"/surrender","aufgeben"},
{"/sweat","schwitzen"},
{"/victory","Siegesschrei"},
{"/woot","freude ausdr�cken"},
{"/wow","staunen"},
{"/yeehaw","Begeistert sein"},
{"/zombie","Zombie"}
}